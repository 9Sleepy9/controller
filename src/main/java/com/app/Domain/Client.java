package com.app.Domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//mark class as an Entity
@Entity
//defining class name as Table name
@Table
public class Client
{
    //mark id as primary key
    @Id
//defining id as column name
    @Column
    private int id;
    //defining trip_name as column name
    @Column
    private String trip;
    //defining name as column name
    @Column
    private String name;
    //defining email as column name
    @Column
    private String email;

    public int getId()
    {
        return id;
    }
    public String getName()
    {
        return name;
    }
    public String getTrip()
    {
        return trip;
    }
    public String getEmail()
    {
        return email;
    }
}