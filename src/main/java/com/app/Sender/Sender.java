package com.app.Sender;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Sender {

    @Autowired
    private RabbitTemplate template;

    @Autowired
    private Queue queue;

    public void send(String clientMessage) {
        this.template.convertAndSend(queue.getName(), clientMessage);
        System.out.println(" [x] Sent '" + clientMessage + "'");
    }
}
